﻿using copypedia.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace copypedia.App_Start
{
    public class DatabaseInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        public DatabaseInitializer() : base() { }
        protected override void Seed(ApplicationDbContext context)
        {
            CreateRoles(context);

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // Create users
            var admin = new ApplicationUser { UserName = "admin@copypedia.org", Email = "admin@copypedia.org" };
            var calin = new ApplicationUser { UserName = "calin@copypedia.org", Email = "calin@copypedia.org" };
            var marian = new ApplicationUser { UserName = "marian@copypedia.org", Email = "marian@copypedia.org" };
            var unknown = new ApplicationUser { UserName = "Unknown" };
            SeedUser(context, userManager, roleManager, admin, "asAS12!@", "Admin");
            SeedUser(context, userManager, roleManager, calin, "asAS12!@", "Editor");
            SeedUser(context, userManager, roleManager, marian, "asAS12!@", "User");
            SeedUser(context, userManager, roleManager, unknown, "asAS12!@", "User");

            // Create categories
            var cat1 = new Category { Name = "History" };
            var cat2 = new Category { Name = "Physics" };

            // Create articles
            AddArticle(context,
                new Article
                {
                    Title = "Siege of Itami (1574)",
                    Content = @"<img src='/Content/Images/117239-200x267-Himeiji_Castle.jpg' /> <br /> <b>The 1574 siege of Itami</b> (伊丹の戦い Itami no Tatakai) was the first time Itami Castle would be attacked. Oda Nobunaga attacked the castle by digging a tunnel under the walls. Araki Murashige was then given the castle.",
                    Category = cat1,
                    Author = calin
                });
            AddArticle(context,
                new Article
                {
                    Title = "Battle of Anegawa",
                    Content = @"<b>The Sengoku period Battle of Anegawa</b> (姉川の戦い Anegawa no Tatakai) (30 July 1570) occurred near Lake Biwa in Ōmi Province, Japan, between the allied forces of Oda Nobunaga and Tokugawa Ieyasu, against the combined forces of the Azai and Asakura clans. It is notable as the first battle that involved the alliance between Nobunaga and Ieyasu, liberated the Oda clan from its unbalanced alliance with the Azai, and saw Nobunaga's prodigious use of firearms. Nobunaga's loyal retainer, Toyotomi Hideyoshi was assigned to lead troops into open battle for the first time.",
                    Category = cat1,
                    Author = marian
                });
            AddArticle(context,
                new Article
                {
                    Title = "Richard Feynman",
                    Content = @"<img src='/Content/Images/Richard_Feynman_Nobel.jpg' /> <br /><b>Richard Phillips Feynman</b> (/ˈfaɪnmən/; May 11, 1918 – February 15, 1988) was an American theoretical physicist, known for his work in the path integral formulation of quantum mechanics, the theory of quantum electrodynamics, and the physics of the superfluidity of supercooled liquid helium, as well as in particle physics for which he proposed the parton model. For his contributions to the development of quantumelectrodynamics, Feynman, jointly with Julian Schwinger and Shin'ichirō Tomonaga, received the Nobel Prize in Physics in 1965. ",
                    Category = cat2,
                    Author = calin
                });
            base.Seed(context);
        }

        private void CreateRoles(ApplicationDbContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            string[] roleNames = new string[] { "Admin", "Editor", "User" };
            foreach (var name in roleNames)
            {
                if (!roleManager.RoleExists(name))
                {
                    roleManager.Create(new IdentityRole { Name = name });
                }
            }
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            
        }

        private void SeedUser(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            ApplicationUser user, string password, string roleName)
        {
            userManager.Create(user, password);
            userManager.AddToRole(user.Id, roleName);
        }

        private void AddArticle(ApplicationDbContext context, Article article)
        {
            var diff = new ArticleDiff
            {
                Article = article,
                Author = article.Author,
                Content = article.Content,
                Message = "Article created"
            };
            article.Diffs.Add(diff);
            context.Articles.Add(article);
        }
    }
}