﻿using System;
using System.ComponentModel.DataAnnotations;

namespace copypedia.Models
{
    public class ArticleDiff
    {
        public int ArticleDiffId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Required]
        [StringLength(50)]
        public string Message { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        public virtual ApplicationUser Author { get; set; }
        public virtual Article Article { get; set; }
    }
}