﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace copypedia.Models
{
    public class Article
    {
        public int ArticleId { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Created on")]
        [DataType(DataType.DateTime)]
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        [Required]
        public bool IsEditable { get; set; } = true;

        public virtual ApplicationUser Author { get; set; }
        public virtual Category Category { get; set; }
        public virtual List<ArticleDiff> Diffs { get; set; } = new List<ArticleDiff>();
    }
}