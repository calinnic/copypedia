﻿using copypedia.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

[assembly: OwinStartupAttribute(typeof(copypedia.Startup))]
namespace copypedia
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void CreateAdminUserAndApplicationRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            string[] roleNames = new string[] { "Admin", "Editor", "User"};
            foreach (var name in roleNames)
            {
                if (!roleManager.RoleExists(name))
                {
                    roleManager.Create(new IdentityRole { Name = name });
                }
            }
            SeedUser(context, userManager, roleManager, "admin@copypedia.org", "asAS12!@", "Admin");
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            SeedUser(context, userManager, roleManager, "admin@copypedia.org", "asAS12!@", "Admin");
            SeedUser(context, userManager, roleManager, "calin@copypedia.org", "asAS12!@", "Editor");
            SeedUser(context, userManager, roleManager, "marian@copypedia.org", "asAS12!@", "User");
        }

        private void SeedUser(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            string email, string password, string roleName)
        {
            var user = new ApplicationUser { UserName = email, Email = email };
            userManager.Create(user, password);
            userManager.AddToRole(user.Id, roleName);
        }
    }
}
