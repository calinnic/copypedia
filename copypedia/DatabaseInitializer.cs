﻿using copypedia.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace copypedia.App_Start
{
    public class DatabaseInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        public DatabaseInitializer() : base() { }
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);
            context.Categories.AddRange((new List<string> { "Mathematics", "Physics", "Computer Science", "Electronics" }).Select(x => new Category(x)));
        }
    }
}