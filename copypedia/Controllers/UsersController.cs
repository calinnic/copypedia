﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using copypedia.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace copypedia.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            user.RoleName = db.Roles.Find(user.Roles.FirstOrDefault().RoleId).Name;
            return View(user);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.AllRoles = from x in db.Roles select x.Name;
            user.RoleName = db.Roles.Find(user.Roles.FirstOrDefault().RoleId).Name;
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,Email,RoleName")] ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                string currentRoleName = (from x in db.Roles where x.Id == user.Roles.FirstOrDefault().RoleId select x.Name).FirstOrDefault();
                if (user.Roles.Count > 0)
                {
                    userManager.RemoveFromRole(user.Id, roleManager.FindById(user.Roles.First().RoleId).Name);
                }
                userManager.AddToRole(user.Id, user.RoleName);
                ApplicationUser existingUser = userManager.FindById(user.Id);
                existingUser.Email = user.Email;
                existingUser.UserName = user.UserName;
                existingUser.RoleName = user.RoleName;
                userManager.Update(existingUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }
    }
}