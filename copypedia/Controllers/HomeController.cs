﻿using copypedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace copypedia.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public PartialViewResult RecentArticles()
        {
            int articlesToShow = 5;
            var RecentArticles = (from x in db.Articles
                                  orderby x.CreationDateTime descending
                                  select x).Take(articlesToShow);
            return PartialView("_RecentArticles", RecentArticles.ToList());
        }

        public PartialViewResult Categories()
        {
            return PartialView("_Categories", db.Categories.ToList());
        }
    }
}