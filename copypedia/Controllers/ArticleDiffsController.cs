﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using copypedia.Models;

namespace copypedia.Controllers
{
    public class ArticleDiffsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ArticleDiffs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleDiff articleDiff = db.ArticleDiffs.Find(id);
            if (articleDiff == null)
            {
                return HttpNotFound();
            }
            return View(articleDiff);
        }

        // POST: ArticleDiffs/Apply/5
        [HttpPost]
        [Authorize(Roles = "Editor,Admin")]
        public ActionResult Apply(int articleDiffId)
        {
            ArticleDiff diff = db.ArticleDiffs.Find(articleDiffId);
            Article article = db.Articles.Find(diff.Article.ArticleId);
            article.Content = diff.Content;
            article.Diffs = article.Diffs.OrderBy(x => x.ArticleDiffId).ToList(); //TODO: Don't sort by IDs because they are not ordered
            article.Diffs.RemoveRange(article.Diffs.LastIndexOf(diff) + 1, article.Diffs.Count - article.Diffs.LastIndexOf(diff) - 1);
            db.Entry(article).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", "Articles");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
