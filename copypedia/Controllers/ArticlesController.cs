﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using copypedia.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace copypedia.Controllers
{
    [Authorize(Roles = "User,Editor,Admin")]
    public class ArticlesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Articles
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Articles.ToList());
        }

        // GET: Articles/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // GET: Articles/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            ViewBag.CategoryNames = db.Categories.Select(x => x.Name);
            return View();
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] // TODO: Enable input validation ??
        [AllowAnonymous]
        public ActionResult Create([Bind(Include = "Title,Content")] Article article, string CategoryName)
        {
            if (ModelState.IsValid)
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                ArticleDiff diff = new ArticleDiff
                {
                    Author = Request.IsAuthenticated ? userManager.FindByName(User.Identity.Name) : userManager.FindByName("Unknown"),
                    Message = "Article created",
                    Content = article.Content,
                    CreationDateTime = DateTime.Now,
                };
                article.Diffs.Add(diff);
                article.Category = db.Categories.Where(x => x.Name == CategoryName).First();
                article.Author = Request.IsAuthenticated ? userManager.FindByName(User.Identity.Name) : userManager.FindByName("Unknown");
                article.CreationDateTime = DateTime.Now;
                db.Articles.Add(article);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(article);
        }

        // GET: Articles/Edit/5
        [AllowAnonymous]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            if (!article.IsEditable && User.Identity.Name != article.Author.UserName)
            {
                ViewBag.Message = "This article has been locked and cannot be edited right now... :(";
                return View("Error");
            }
            return View(article);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] // TODO: Enable input validation ??
        [AllowAnonymous]
        public ActionResult Edit([Bind(Include = "ArticleId,Title,Content")] Article x, string message)
        {
            if (ModelState.IsValid)
            {
                Article article = db.Articles.Find(x.ArticleId);
                if (!article.IsEditable && !Request.IsAuthenticated)
                {
                    // TODO: Show that article is not editable instead of BadRequest
                    ViewBag.Message = "This article has been locked and cannot be edited right now :(";
                    return View("Error");
                }
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                ArticleDiff diff = new ArticleDiff
                {
                    Author = Request.IsAuthenticated ? userManager.FindByName(User.Identity.Name) : userManager.FindByName("Unknown"),
                    Message = message,
                    Content = x.Content,
                    CreationDateTime = DateTime.Now,
                };
                article.Diffs.Add(diff);
                article.Title = x.Title;
                article.Content = x.Content;
                //db.ArticleDiffs.Add(diff);
                db.Entry(article).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(x);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Article article = db.Articles.Find(id);
            article.Diffs.Clear();
            db.Articles.Remove(article);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult Search(string s)
        {
            var q = from x in db.Articles
                    where x.Title.Contains(s) || x.Content.Contains(s)
                    select x;
            return View(q);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetEditable([Bind(Include = "ArticleId,IsEditable")] Article x)
        {
            Article article = db.Articles.Find(x.ArticleId);
            if (article.Author.UserName != User.Identity.Name && User.IsInRole("User"))
            {
                ViewBag.Message = "This article has been locked and cannot be edited right now :(";
                return View("Error");
            }
            article.IsEditable = x.IsEditable;
            db.Entry(article).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details", new { id = article.ArticleId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
